const fs = require('fs');
const path = require('path');

function createRandomJSONFiles(directory, count, callback) {
    fs.mkdir(directory, (mkdirErr) => {
        if (mkdirErr) {
            callback(mkdirErr);
            return;
        }

        const createFile = (index) => {
            if (index === count) {
                callback(null, 'Random JSON files created successfully');
                return;
            }

            const filename = `file${index}.json`;
            const filepath = path.join(directory, filename);
            console.log(filepath, " file created");
            const data = { randomData: Math.random() };

            fs.writeFile(filepath, JSON.stringify(data), (writeErr) => {
                if (writeErr) {
                    callback(writeErr);
                    return;
                }

                createFile(index + 1);
            });
        };

        createFile(0);
    });
}

function deleteFiles(directory, callback) {
    // getListOfFiles("./");
    console.log()
    fs.rm(directory, { recursive: true }, (err) => {
        if (err) {
            callback(err);
            return;
        }
        callback(null, 'File deleted successfully');

        console.log()
    });
}

// function getListOfFiles(directory) {
//     fs.readdir(directory, (err, files) => {
//         if (err)
//             console.log(err);
//         else {
//             console.log("\nCurrent directory filenames:");
//             files.forEach(file => {
//                 console.log(file);
//             })
//         }
//     })
// }

module.exports = { createRandomJSONFiles, deleteFiles }
