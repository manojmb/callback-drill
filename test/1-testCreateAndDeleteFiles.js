const { createRandomJSONFiles, deleteFiles } = require('../1-createAndDeleteFiles.js');
const directoryPath = './output';
const fs = require('fs');

try {
    createRandomJSONFiles(directoryPath, 5, (err, message) => {
        if (err) {
            console.error('Error creating random JSON files:', err);
            return;
        } else {

            console.log(message);
            deleteFiles(directoryPath, (err, deleteMessage) => {
                if (err) {
                    console.error('Error deleting files:', err);
                    return;
                }

                console.log(deleteMessage);


            });
        }
    });
} catch (e) {
    console.log(e.message);
}
