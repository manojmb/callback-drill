const { readFileLipsum, upperCaseAndWriteFile, lowerCaseAndWriteFile, sortFiles, deleteFilesInTextFile } = require("../2-lipsumManipulation");
path = "../lipsum_1.txt"
// const filepath = "../output/filenames.txt"

try {
    readFileLipsum(path, (err, lipsumData) => {
        if (err) {
            console.error("Error:", err);
            return;
        }
        else {
            upperCaseAndWriteFile(lipsumData, (upperErr, upperMessage) => {
                if (upperErr) {
                    console.log(upperErr);
                    return;
                }
                console.log(upperMessage);

                lowerCaseAndWriteFile(lipsumData, (lowerErr, lowerMessage) => {
                    if (lowerErr) {
                        console.log(lowerErr);
                        return;
                    }
                    console.log(lowerMessage);
                    sortFiles((err, result) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('Merge successful:', result);
                        }

                        deleteFilesInTextFile();

                    })
                });
            });
        }
    });
} catch (e) {
    console.log(e.message)
}




