const fs = require("fs");
const { deleteFiles } = require("./1-createAndDeleteFiles");

const readFileLipsum = (filePath, callback) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) {
            console.error("Error while reading:", err);
            callback(err, null);
            return;
        }
        callback(null, data);
    });
};

function deleteFilesInTextFile() {

    fs.readFile('../filenames.txt', 'utf-8', (readErr, filenamesContent) => {
        if (readErr) {
            console.error('Error reading filenames.txt:', readErr);
            return;
        } else {
            const filenamesArray = filenamesContent.split('\n').map(filename => filename.trim());

            if (filenamesArray.length === 0) {
                console.log('No filenames found in filenames.txt');
            } else {
                // console.log(filenamesArray)
                filenamesArray.forEach((filename) => {
                    deleteFiles("../output/" + filename, (err, deleteMessage) => {
                        if (err) {
                            console.error('Error deleting files:', err);
                            return;
                        }
                        else {
                            console.log(deleteMessage);
                            console.log("../output/" + filename)
                        }

                    });
                })
            }

        }
    });
}



function upperCaseAndWriteFile(lipsumdata, callback) {
    fs.mkdir("../output", (mkdirErr) => {
        if (mkdirErr) {
            callback(mkdirErr);
            return;
        }
    })
    fs.writeFile("../output/upperCaseData.txt", lipsumdata.toUpperCase(), (writeErr) => {
        if (writeErr) {
            callback(writeErr);
        } else {
            fs.appendFile("../filenames.txt", `upperCaseData.txt\n`, 'utf8', (appendErr) => {
                if (appendErr) {
                    callback(appendErr);
                } else {
                    callback(null, 'Created Uppercasefile');
                }
            });
        }
    });
}

function sortFiles(callback) {
    fs.readFile("../output/lowerCaseData.txt", 'utf8', (err1, content1) => {
        if (err1) {
            callback(err1);
            return
        } else {
            fs.readFile("../output/upperCaseData.txt", 'utf8', (err2, content2) => {
                if (err2) {
                    callback(err2);
                    return
                }


                let mergedContent = (content1 + '\n' + content2).split('\n');

                let sortedContent = mergedContent.map((element) => {
                    return element.trim()
                }).filter((element) => element != "").sort().join("\n");


                fs.writeFile('../output/sortedContent.txt', sortedContent, 'utf8', (err3) => {
                    if (err3) {
                        callback(err3);
                        return
                    } else {
                        fs.appendFile("../filenames.txt", `sortedContent.txt`, 'utf8', (appendErr) => {
                            if (appendErr) {
                                callback(appendErr)
                                return;
                            } else {
                                callback("sort successful")
                            }
                        })
                    }

                });
            });
        }


    });
}

function lowerCaseAndWriteFile(data, callback) {
    const lowerCaseData = data.toLowerCase();

    // Split into sentences (assuming sentences end with '.', '!', or '?')
    const sentences = lowerCaseData.split(/[.!?]/);

    // Remove empty sentences
    const filteredSentences = sentences.filter(sentence => sentence.trim() !== '');

    // Write to a new file
    const newContent = filteredSentences.join('\n');

    fs.writeFile("../output/lowerCaseData.txt", newContent, 'utf8', (writeErr) => {
        if (writeErr) {
            callback(writeErr);
        } else {
            fs.appendFile("../filenames.txt", `lowerCaseData.txt\n`, 'utf8', (appendErr) => {
                if (appendErr) {
                    callback(appendErr);
                } else {
                    callback(null, 'Lowercase file created');
                }
            });
        }
    });
}

module.exports = { readFileLipsum, upperCaseAndWriteFile, lowerCaseAndWriteFile, sortFiles, deleteFilesInTextFile };